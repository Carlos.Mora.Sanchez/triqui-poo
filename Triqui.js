let turno = "X";
let nJugadas = 0;

function jugar() {
  let i;
  let Tablero = document.getElementById("tablero");

  for (i = 1; i < 10; i++) {
    let Casilla = document.createElement("input");
    Casilla.value = " ";
    Casilla.setAttribute("type", "button");
    Casilla.setAttribute("class", "casilla");
    Casilla.setAttribute("id", "casilla" + i);
    Casilla.setAttribute("onClick", "hacerJugada(this.id)");
    Tablero.appendChild(Casilla);

    if (i % 3 == 0) {
      let espacio = document.createElement("br");
      Tablero.appendChild(espacio);
    }
  }
}

function hacerJugada(idCasilla) {
  let resultado, triqui;
  resultado = marcar(idCasilla);

  if (resultado == true) {
    nJugadas++;
    triqui = verificarTriqui();
    if (triqui) {
      alert("El Ganador es " + turno + ", felicidades");
    } else if (nJugadas == 9) {
      alert("Empate");
    } else {
      cambiarTurno();
    }
  }
  return false;
}

function cambiarTurno() {
  if (turno === "X") {
    turno = "O";
  } else {
    turno = "X";
  }
}

function marcar(idCasilla) {
  let casilla = document.getElementById(idCasilla);

  if (casilla.value === "X" || casilla.value === "O") {
    alert(
      "La casilla se encuentra ocupada, seleccionar una casilla disponible"
    );
    return false;
  }
  casilla.value = turno;

  return true;
}

function verificarTriqui() {
  let i;
  let casilla1;
  let casilla2;
  let casilla3;

  for (i = 0; i < 9; i = i + 3) {
    casilla1 = document.getElementById("casilla" + (i + 1)).value;
    casilla2 = document.getElementById("casilla" + (i + 2)).value;
    casilla3 = document.getElementById("casilla" + (i + 3)).value;
    if (casilla1 === casilla2 && casilla1 === casilla3 && casilla1 != " ") {
      return true;
    }
  }

  for (i = 1; i < 4; i++) {
    casilla1 = document.getElementById("casilla" + i).value;
    casilla2 = document.getElementById("casilla" + (i + 3)).value;
    casilla3 = document.getElementById("casilla" + (i + 6)).value;
    if (casilla1 === casilla2 && casilla1 === casilla3 && casilla1 != " ") {
      return true;
    }
  }

  let valor = [2, 4];
  for (i = 0; i < 2; i++) {
    casilla1 = document.getElementById("casilla" + 5).value;
    casilla2 = document.getElementById("casilla" + (5 + valor[i])).value;
    casilla3 = document.getElementById("casilla" + (5 - valor[i])).value;
    if (casilla1 === casilla2 && casilla1 === casilla3 && casilla1 != " ") {
      return true;
    }
  }

  return false;
}

function borrar() {
  nJugadas = 0;
  turno = "X";
  for (i = 1; i < 10; i++) {
    casilla = document.getElementById("casilla" + i);
    casilla.value = " ";
  }
}
